let link = document.getElementById('styleTheme');
document.addEventListener('DOMContentLoaded', function () {
    if (window.localStorage.getItem('themeStyle') === null) {
        link.href = 'css/styleLight.css';
        window.localStorage.setItem('themeStyle', 'css/styleLight.css');
    }
    else {
        link.href = window.localStorage.getItem('themeStyle');
    }
});
function save() {
    if (window.localStorage.getItem('themeStyle') === 'css/styleLight.css') {
        link.href = 'css/styleDark.css';
        window.localStorage.setItem('themeStyle', 'css/styleDark.css');
    }
    else {
        link.href = 'css/styleLight.css';
        window.localStorage.setItem('themeStyle', 'css/styleLight.css');
    }
}
document.getElementById('theButton').addEventListener('click', save);
document.onunload = function () {
    link.href = window.localStorage.getItem('themeStyle');
}